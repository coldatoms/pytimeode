import pickle
import warnings

import numpy as np

import pytest

from .minimal_example import State, StateNumexpr, test_classes
from ..evolvers import EvolverABM


class Test(object):
    def y(self, t):
        y = np.array([np.exp(1j * (t - 1) ** 2)])
        y.dtype = float
        return y

    def test_pickle(self):
        """Test that the minimal states can be pickled."""
        y0 = State()
        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(steps=100)
        y1 = pickle.loads(pickle.dumps(e.y))
        assert np.allclose(e.y.t, y1.t)
        assert np.allclose(e.y.data, y1.data)

    def test_no_numexpr(self):
        y0 = State()
        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(steps=100)
        # y = (e.y.data, self.y(t=e.t))
        assert np.allclose(e.y.data, self.y(t=e.t))

    def test_numexpr(self):
        y0 = StateNumexpr()
        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(steps=100)
        # y = (e.y.data, self.y(t=e.t))
        assert np.allclose(e.y.data, self.y(t=e.t))

    def test_zeros(self):
        y0 = State()
        y0.t = 1.2
        y0.data[...] = 1.0
        y1 = y0.zeros()
        assert np.allclose(y1.data[...], 0.0)
        assert np.allclose(y1.t, 1.2)
        y1.data[...] = 2.0
        assert np.allclose(y0.data[...], 1.0)

    def test_evolver_t(self):
        y0 = State()
        y0.t = 0.0
        e = EvolverABM(y=y0, dt=0.01, t=1.2)
        assert np.allclose(e.y.t, 1.2)

    def test_compute_dy_issue_18(self):
        """Test backwards compatibility code"""
        OldState = type("OldState", State.__bases__, dict(State.__dict__))
        OldState.compute_dy = OldState.compute_dy_dt
        del OldState.compute_dy_dt

        y0 = OldState()

        # It should raise the warning
        warnings.simplefilter("error", DeprecationWarning)
        with pytest.raises(DeprecationWarning):
            e = EvolverABM(y=y0, dt=0.01)

        # but if only a warning, should work...
        warnings.simplefilter("always", DeprecationWarning)

        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(steps=100)
        assert np.allclose(e.y.data, self.y(t=e.t))


class TestCoverage(object):
    """Some tests to help with coverage."""

    def test_no_normalize(self):
        y0 = State()
        with pytest.raises(ValueError):
            e = EvolverABM(y=y0, dt=0.01, normalize=True)
            assert np.allclose(e.y.t, y0.t)

    def test_get_dy(self):
        y0 = State()
        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(10)
        assert np.allclose(e.get_dy().data, e.get_dy(y=e.y).data)

    def test_pickle(self):
        """Test pickling of evolvers."""
        y0 = State()
        e = EvolverABM(y=y0, dt=0.01)
        e.evolve(10)
        e1 = pickle.loads(pickle.dumps(e))
        e.evolve(10)
        e1.evolve(10)
        assert np.allclose(e.y.data, e1.y.data)
        assert np.allclose(e.y.t, e1.y.t)
