import warnings

import numpy as np

from .test_split import State

from .. import evolvers

import pytest


def pytest_generate_tests(metafunc):
    if "Evolver" in metafunc.fixturenames:
        metafunc.parametrize(
            "Evolver", [getattr(evolvers, _Evolver) for _Evolver in evolvers.__all__]
        )


def test_issue_21(Evolver):
    """Regression test for issue #21: Evolver `state` and `get_state()`"""
    y0 = State()
    e = Evolver(y=y0, dt=0.01)
    e.evolve(steps=10)

    # It should raise the warning
    warnings.simplefilter("error", DeprecationWarning)
    with pytest.raises(DeprecationWarning):
        e.state

    with pytest.raises(DeprecationWarning):
        e.get_state()

    # but if only a warning, should work...
    warnings.simplefilter("always", DeprecationWarning)

    assert e.y is e.state
    assert np.allclose(e.get_y().data, e.get_state().data)
