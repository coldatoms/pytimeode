"""Minimal Example for testing and demonstrating how to us the interfaces.

Here we solve the following zero-dimensional problem:

.. math::
   y = a e^{-i t}
   y' = -i y
   y'' = - y

This corresponds to evolution with potential $V = 1$
"""

from __future__ import division

import numpy as np
from mmfutils import interface

from pytimeode import interfaces, mixins, evolvers
from pytimeode.utils import testing

import pytest


@interface.implementer(
    interfaces.IStateForABMEvolvers, interfaces.IStateForSplitEvolvers
)
class State(mixins.ArrayStateMixin):
    def __init__(self, a=2.0):
        self.a = a
        self.t = 0.0
        self.data = np.array([1.0], dtype=complex)

    def pre_evolve_hook(self):
        # Normalize the magnitude of the state
        self.data *= abs(self.a / self.data)

    @property
    def exact(self):
        return self.a * np.exp(-1j * self.t)

    ######################################################################
    # IStateForABMEvolvers
    def compute_dy_dt(self, dy):
        dy[...] = -1j * self[...]
        return dy

    ######################################################################
    # IStateForSplitEvolvers
    linear = False

    def apply_exp_K(self, dt):
        r"""Apply $e^{-i K dt}$ in place"""
        pass

    def apply_exp_V(self, dt, state):
        r"""Apply $e^{-i V dt}$ in place using `state` for any
        nonlinear dependence in V. (Linear problems should ignore
        `state`.)"""
        V = 1.0
        self.data *= np.exp(-1j * V * dt)


@pytest.fixture
def state():
    yield State()


def test_check_split_operator(state):
    ts = testing.TestState(state)
    assert all(ts.check_split_operator())


def test_state(state):
    N = 10
    T = 1.1
    e = evolvers.EvolverABM(state, dt=T / N)
    e.evolve(N)
    assert np.allclose(e.y[...], e.y.exact)

    e = evolvers.EvolverSplit(state, dt=T / N)
    e.evolve(N)
    assert np.allclose(e.y[...], e.y.exact)
