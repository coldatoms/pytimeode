import math

import numpy as np
from scipy.linalg import expm

import pytest

from ..evolvers import EvolverABM, EvolverSplit
from ..utils import testing
from ..interfaces import (
    implementer,
    IStateForABMEvolvers,
    IStateForSplitEvolvers,
    IStateWithNormalize,
)

from ..mixins import ArrayStateMixin

__all__ = ["State"]


@implementer(IStateForABMEvolvers, IStateForSplitEvolvers)
class State_(ArrayStateMixin):
    def __init__(self, N=2, normalize=False):
        np.random.seed(1)
        self.K = np.random.random((N, N)) + 1j * np.random.random((N, N)) - 0.5 - 0.5j
        self.V = np.random.random((N, N)) + 1j * np.random.random((N, N)) - 0.5 - 0.5j
        self.y0 = np.random.random(N) + 1j * np.random.random(N) - 0.5 - 0.5j

        self.data = self.y0.copy()

        # Make operators Hermitian so norm is preserved if testing with
        # normalize
        if normalize:
            self.K += self.K.T.conj()
            self.V += self.V.T.conj()

            # Compute chemical potential and subtract from mu so that phase does
            # not evolve.  This allows us to test with normalization.
            Hy = self.empty()
            self.compute_dy_dt(dy=Hy)
            Hy *= 1j
            y, Hy = self[...], Hy[...]
            mu = (
                (y.conj().ravel().dot(Hy.ravel())).sum()
                / (y.conj().ravel().dot(y.ravel())).sum()
            ).real
            self.V -= mu

    def compute_dy_dt(self, dy):
        """Return `dy/dt` at time `self.t`.

        If `dy` is provided, then use it for the result, otherwise return a new
        state.
        """
        dy[...] = (self.K + self.V).dot(self[...]) / 1j
        return dy

    def get_potentials(self):
        """Return `potentials` at time `self.t`."""

    def apply_exp_K(self, dt):
        r"""Apply $e^{-i K dt}$ in place"""
        self[...] = expm(self.K / 1j * dt).dot(self[...])

    def apply_exp_V(self, dt, state):
        r"""Apply $e^{-i V dt}$ in place"""
        self[...] = expm(self.V / 1j * dt).dot(self[...])

    def get_y_exact(self, t):
        H = self.K + self.V
        return expm(-1j * t * H).dot(self.y0)


@implementer(IStateWithNormalize)
class State(State_):
    """Provides methods for normalizing."""

    def pre_evolve_hook(self):
        """Save this before evolving."""
        self._N = self.get_N()

    def get_N(self):
        return (abs(self.data) ** 2).sum()

    def get_normalization(self):
        return math.sqrt(self._N / self.get_N())


def pytest_generate_tests(metafunc):
    """Generate tests for each value of `normalize`"""
    if "normalize" in metafunc.fixturenames:
        metafunc.parametrize("normalize", [False, True])


class Test(object):
    def y(self, t):
        y = np.array([np.exp(1j * (t - 1) ** 2)])
        y.dtype = float
        return y

    def test_no_numexpr(self, normalize):
        y0 = State(normalize=normalize)
        e1 = EvolverABM(y=y0, dt=0.01, normalize=normalize)
        e1.numexpr = None
        e2 = EvolverSplit(y=y0, dt=0.005, normalize=normalize)
        e2.numexpr = None
        e1.evolve(steps=10)
        e2.evolve(steps=20)
        assert np.allclose(e1.y.t, e2.y.t)
        assert np.allclose(e1.y.data, e2.y.data)

    def test_numexpr(self, normalize):
        y0 = State(normalize=normalize)
        e1 = EvolverABM(y=y0, dt=0.01, normalize=normalize)
        e2 = EvolverSplit(y=y0, dt=0.005, normalize=normalize)
        e1.evolve(steps=10)
        e2.evolve(steps=20)
        assert np.allclose(e1.y.t, e2.y.t)
        assert np.allclose(e1.y.data, e2.y.data)

    def test_testing(self, normalize):
        y = State(normalize=normalize)
        t = testing.TestState(y)
        assert all(t.check_split_operator(normalize=normalize))

    def test_testing_issue_18(self):
        OldState = type("OldState", State_.__bases__, dict(State_.__dict__))
        OldState.compute_dy = OldState.compute_dy_dt
        del OldState.compute_dy_dt

        y = OldState(normalize=False)
        t = testing.TestState(y)
        assert all(t.check_split_operator(normalize=False))


class TestCoverage(object):
    """Coverage tests for exceptional cases in check_split_operator()"""

    def test_normalize_exception(self):
        t = testing.TestState(State_())
        with pytest.raises(ValueError):
            assert all(t.check_split_operator(normalize=True))

    def test_normalize_None(self):
        t = testing.TestState(State(normalize=False))

        # This will fail if normalize is set to True since the operator is not
        # Hermitian
        with pytest.raises(AssertionError):
            assert all(t.check_split_operator(normalize=None))
