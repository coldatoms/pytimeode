import pytest

import numpy as np
from zope.interface.exceptions import BrokenImplementation, BrokenMethodImplementation

from mmfutils.interface import implementer, verifyClass, verifyObject


from ..interfaces import IStateForABMEvolvers
from ..mixins import ArrayStateMixin


@implementer(IStateForABMEvolvers)
class State0(ArrayStateMixin):
    """Incomplete State to test interface checking"""

    def __init__(self, N=4, dim=2):
        self.N = N
        self.dim = dim
        self.data = np.zeros((self.N,) * self.dim, dtype=complex)

    # Missing method compute_dy_dt


@implementer(IStateForABMEvolvers)
class State1(State0):
    """Broken State to test interface checking"""

    def compute_dy_dt(self):
        # Missing argument dy
        self.data[...] = -self.data


@implementer(IStateForABMEvolvers)
class State(State1):
    """Correct State to test interface checking"""

    def compute_dy_dt(self, dy):
        dy.data[...] = -self.data


class TestInterfaces(object):
    def test_missing_method(self):
        with pytest.raises(BrokenImplementation) as err:
            verifyClass(IStateForABMEvolvers, State0)
            mess = str(err.value)
            assert "An object has failed to implement interface" in mess
            assert "The compute_dy_dt attribute was not provided." in mess

    def test_broken_method(self):
        with pytest.raises(BrokenMethodImplementation) as err:
            verifyClass(IStateForABMEvolvers, State1)
            mess = str(err.value)
            assert "The implementation of compute_dy_dt violates its contract" in mess
            assert "because implementation doesn't allow enough arguments." in mess

    def test_class_interface(self):
        verifyClass(IStateForABMEvolvers, State)

    def test_object_interface(self):
        verifyObject(IStateForABMEvolvers, State())


class TestInterfacesDoctests(object):
    """Doctests to ensure reasonable error messages are given.

    >> verifyClass(IStateForABMEvolvers, State0)
    Traceback (most recent call last):
       ...
    BrokenImplementation: An object has failed to implement interface \
             <InterfaceClass pytimeode.interfaces.IStateForABMEvolvers>
    <BLANKLINE>
            The compute_dy_dt attribute was not provided.
    <BLANKLINE>

    >> verifyClass(IStateForABMEvolvers, State1)
    Traceback (most recent call last):
       ... State1 ...
    BrokenMethodImplementation: The implementation of compute_dy_dt violates\
     its contract because implementation doesn't allow enough arguments.

    >> verifyClass(IStateForABMEvolvers, State)
    True

    >> s = State()
    >> verifyObject(IStateForABMEvolvers, s)
    True

    >> s.writable
    Traceback (most recent call last):
       ...
    AttributeError: Cannot get attribute `writable`.  Did you mean `writeable`?

    >> s.writable = False
    Traceback (most recent call last):
       ...
    AttributeError: Cannot set attribute `writable`.  Did you mean `writeable`?
    """
