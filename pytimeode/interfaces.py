"""Interfaces.

At the top level, one probably wants to use the tested evolvers in
``evolver.py``.  These require only an object implementing the appropriate
``IState`` interface.

If you want to reuse other components like bases, then you will need to
implement the additional interfaces define here.  Here is the dependency graph.
"""

from mmfutils.interface import implementer, Interface, Attribute

__all__ = [
    "IEvolver",
    "IStateMinimal",
    "IState",
    "IStateFlat",
    "IStateAsArray",
    "INumexpr",
    "IStateForABMEvolvers",
    "IStateForSplitEvolvers",
    "IStatePotentialsForSplitEvolvers",
    "IStateWithNormalize",
    "implementer",
]


class IEvolver(Interface):
    """General interface for evolvers"""

    y = Attribute("y", "Current state")
    t = Attribute("t", "Current time (deprecated - used y.t instead)")

    def __init__(y, dt, t=0.0, copy=True):
        """Return an evolver starting with state `y` at time `t` and evolve
        with step `dt`."""

    def evolve(steps):
        """Evolve the initial state by `steps` of length `dt` in time"""

    # Convenience methods
    def get_y():
        """Return a copy of the current state `y`."""


class IStateMinimal(Interface):
    """Minimal interface required for state objects.  This will not satisfy all
    uses of a state."""

    writeable = Attribute(
        "writeable",
        """Set to `True` if the state is writeable, or `False` if the state
        should only be read.""",
    )

    dtype = Attribute(
        "dtype",
        """Return the dtype of the underlying state.  If this is real, then it
        is assumed that the states will always be real and certain
        optimizations may take place.""",
    )

    t = Attribute(
        "t",
        """Time at which state is valid.  This is the time at which potentials
        should be evaluated etc.  (It will be set by the evolvers before
        calling the various functions like compute_dy_dt().)""",
    )

    def copy():
        """Return a writeable copy of the state."""

    def copy_from(y):
        """Set this state to be a copy of the state `y`"""

    def axpy(x, a=1):
        """Perform `self += a*x` as efficiently as possible."""

    def scale(f):
        """Perform `self *= f` as efficiently as possible."""

    # Note: we could get away with __imul__ but it requires one return self
    # which can be a little confusing, so we allow the user to simply define
    # `axpy` and `scale` instead.


class IState(IStateMinimal):
    """Interface required by the evolvers.

    Many of these functions are for convenience, and can be implemented from
    those defined in ``IState`` by including the ``StateMixin`` mixin.
    """

    def __pos__():
        """`+self`"""

    def __neg__():
        """`-self`"""

    def __imul__(f):
        """`self *= f`"""

    def __iadd__(y):
        """`self += y`"""

    def __isub__(y):
        """`self -= y`"""

    def __idiv__(f):
        """`self /= f`"""

    def __itruediv__(f):
        """`self /= f`"""

    def __add__(y):
        """Return `self + y`"""

    def __sub__(y):
        """Return `self - y`"""

    def __mul__(f):
        """Return `self * y`"""

    def __rmul__(f):
        """Return `self * y`"""

    def __truediv__(f):
        """Return `self / y`"""

    def __div__(f):
        """Return `self / y`"""

    def empty():
        """Return a writeable but uninitialized copy of the state.

        Can be implemented with `self.copy()` but some states might be
        able to make a faster version if the data does not need to be copied.
        """

    def zeros():
        """Return a writeable but zeroed out copy of the state.

        Can be implemented with `self.copy()` but some states might be
        able to make a faster version if the data does not need to be copied.
        """

    lock = Attribute(
        "lock",
        """Context manager which locks the state.  Ideally this should prevent
        the data from change, but not all implementations will allow for this
        functionality.  It is mainly used to ensure robust code, rather than an
        essential feature.  (The evolvers will always lock a state when it is
        expected that the state should be be mutated.).""",
    )


class IStateFlat(IState):
    """Extended interface with flat attribute allowing the data to be inspected
    as a 1D array.  This functionality is used by some of the testing and
    utilities, but is not needed by the actual evolvers so implementing it is
    optional but recommended.
    """

    flat = Attribute(
        "flat",
        """An iterator allows iterating over the state data as if it were a 1-D
        array, either in a for-loop or by calling its next method.

        To use this to covert a state to a flat array, you can use the
        following code::

            np.fromiter(state.flat, state.dtype)

        """,
    )


class IStateAsArray(IState):
    """Extended interface exposing the __array_interface__ allowing the state to be
    efficiently converted into an array.  This functionality is used by some of
    the testing and utilities, but is not needed by the actual evolvers so
    implementing it is optional but recommended.
    """

    __array_interface__ = Attribute(
        "__array_interface__",
        """Allows states to act as arrays with ``np.asarray(state)``.""",
    )


class INumexpr(Interface):
    """Allows for numexpr optimizations"""

    dtype = Attribute(
        "dtype",
        """Return the dtype of the underlying state.  If this is real, then it
        is assumed that the states will always be real and certain optimizations
        may take place.""",
    )

    def apply(expr, **kwargs):
        """Evaluate the expression using the arguments in ``kwargs`` and store
        the result in ``self``.  For those instance of the class in ``kwargs``,
        the expression must be applied over all components.  This is used by
        the ``utils.expr.Expression`` class to allow numexpr expressions to be
        applied to custom state objects.
        """


class IStateForABMEvolvers(IState):
    """Interface required by ABM and similar integration based evolvers.

    These evolvers are very general, requiring only the ability for the problem
    to compute $dy/dt$.
    """

    def compute_dy_dt(dy):
        """Return `dy/dt` at time `self.t` using the memory in state `dy`."""


class IStateForSplitEvolvers(IState):
    r"""Interface required by Split Operator evolvers.

    These evolvers assume the problem can be split into two operators - $K$
    (kinetic energy) and $V$ (potential energy) so that $i dy/dt = (K+V)y$.
    The method requires that each of these operators be exponentiated.  The
    approach uses a Trotter decomposition that provides higher order accuracy,
    but requires evaluation of the potentials at an intermediate time.

    This interface requires that the `apply_exp_V()` method accept
    another state object which should be used for calculating any
    non-linear terms in $V$ which are state dependent.

    If your problem is linear (i.e. $V$ depends only on time, not on
    the state as in the case of the usual linear Schrodinger
    equation), then you should set the linear attribute which will
    improve performance (but do not use this for non-linear problems
    or the order of convergence will be reduced).
    """

    linear = Attribute("linear", "Is the problem linear?")

    def apply_exp_K(dt):
        r"""Apply $e^{-i K dt}$ in place"""

    def apply_exp_V(dt, state):
        r"""Apply $e^{-i V dt}$ in place using `state` for any
        nonlinear dependence in V. (Linear problems should ignore
        `state`.)"""


class IStatePotentialsForSplitEvolvers(IStateForSplitEvolvers):
    r"""Interface required by Split Operator evolvers.

    This is a specialization of `IStateForSplitEvolvers` that uses an
    alternative method `get_potentials()` to compute the non-linear
    portion of the potential. It is intended for use when the state is
    much more complicated than the non-linear portion of the
    potential, hence only a separate copy of the potentials is maintained.
    """

    def get_potentials():
        """Return `potentials` at time `self.t`."""

    def apply_exp_V(dt, potentials):
        r"""Apply $e^{-i V dt}$ in place using `potentials`"""


class IStateWithNormalize(IState):
    """Interface for states with a normalize function.  Solvers can then
    provide some extra features natively like allowing imaginary time evolution
    for initial state preparation."""

    def normalize():
        """Normalize (and orthogonalize) the state.  Return the factor `s`.

        This method may be called by the evolvers if they implement non-unitary
        evolution (imaginary time cooling for example) after each step.  For
        Fermionic DFTs, the single-particle wavefunctions would then also need
        to be orthogonalized.

        The normalization factor `s` should be some type of object such that
        multiplying by this object will normalize the state.  In general, this
        will simply be a float or an array, but this will allow for future use
        with more complicated scenarios.
        """
