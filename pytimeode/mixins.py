"""Mixins

These mixins implement many of the required operations using only the methods
required by the Minimal interfaces.  None of the mixins define constructors or
store data, so they can simply be used to provide additional functions so as to
reduce repeated code.  Note that if you desire the mixin class to overload
another method, you must inherit it first.
"""

from __future__ import absolute_import, division, print_function

import collections
import contextlib
import copy
import itertools

import numpy as np

from .interfaces import IState, INumexpr, IStateFlat, IStateAsArray, implementer
from . import interfaces

__all__ = ["StateMixin", "ArrayStateMixin", "ArraysStateMixin"]


@implementer(IState)
class StateMixin(object):
    linear = False  # By default assume problems are nonlinear

    # Note: we could get away with __imul__ but it requires one return self
    # which can be a little confusing, so we allow the user to simply define
    # `axpy` and `scale` instead.
    def __pos__(self):
        """`+self`"""
        return self

    def __neg__(self):
        """`-self`"""
        return -1 * self

    def __imul__(self, f):
        """`self *= f`"""
        self.scale(f)
        return self

    def __iadd__(self, y):
        """`self += y`"""
        assert isinstance(y, self.__class__)
        self.axpy(y)
        return self

    def __isub__(self, y):
        """`self -= y`"""
        assert isinstance(y, self.__class__)
        self.axpy(y, a=-1)
        return self

    def __itruediv__(self, f):
        """`self /= f`"""
        self.scale(1.0 / f)
        return self

    __idiv__ = __itruediv__

    def __add__(self, y):
        """Return `self + y`"""
        assert isinstance(y, self.__class__)
        res = self.copy()
        res.axpy(y)
        return res

    def __sub__(self, y):
        """Return `self - y`"""
        assert isinstance(y, self.__class__)
        res = self.copy()
        res.axpy(y, -1)
        return res

    def __mul__(self, f):
        """Return `self * y`"""
        res = self.copy()
        res.scale(f)
        return res

    __rmul__ = __mul__

    def __truediv__(self, f):
        """Return `self / y`"""
        res = self.copy()
        res.scale(1.0 / f)
        return res

    __div__ = __truediv__

    @property
    @contextlib.contextmanager
    def lock(self):
        writeable = self.writeable
        self.writeable = False
        try:
            yield
        finally:
            self.writeable = writeable

    def empty(self):
        return self.copy()

    def zeros(self):
        res = self.copy()
        res.scale(0)
        return res

    # Here we disable `writeable with a useful error message.  This is
    # a common misspelling that does not agree with our interface.  We
    # do this in __getattr__ rather than with a property so that it
    # does not appear when tab completing.
    _disabled_attributes = set(["writable"])

    def __getattr__(self, name):
        if name in self._disabled_attributes:
            raise AttributeError(
                "Cannot get attribute `{}`.  Did you mean `writeable`?".format(name)
            )
        self.__getattribute__(name)

    def __setattr__(self, name, value):
        if name in self._disabled_attributes:
            raise AttributeError(
                "Cannot set attribute `{}`.  Did you mean `writeable`?".format(name)
            )
        super(StateMixin, self).__setattr__(name, value)

    def normalize(self):
        """Default version of `normalize()` that uses the result of a method
        `get_normalize()` which returns the factor `s` without modifying the
        current state."""
        s = self.get_normalization()
        self *= s
        return s


@implementer(IState, IStateFlat, INumexpr)
class DataHelperMixin(object):
    """Mixin class that provides helper methods for accessing data stored in an
    attribute ``self.data`` which is a dict or list of either arrays or states.

    If one desires to store the data in a different representation, then these
    methods must be reimplemented.
    """

    data = None

    def __repr__(self):
        """We can't really do this since we don't know the constructor.  We
        just show the data here."""
        return "{}(t={}, data={})".format(
            self.__class__.__name__,
            np.array2string(np.array([self.t]))[1:-1],  # Use numpy formatting
            repr(self.data),
        )

    def __iter__(self):
        """Return the list of quantum numbers.

        This version assumes `self.data` is either a Sequence or a Mapping.
        """
        if isinstance(self.data, collections.Sequence):
            return range(len(self.data)).__iter__()
        else:
            return self.data.__iter__()

    def __getitem__(self, key):
        """Return the data associated with `key`.

        This version assumes `self.data` is either a Sequence or a Mapping.
        """
        return self.data[key]

    def copy(self):
        """Return a copy of the state.

        Uses `copy.copy()` to shallow-copy attributes, and copy.deepcopy()` to
        copy the data.
        """
        y = copy.copy(self)
        y.data = copy.deepcopy(self.data)
        y.writeable = True  # Copies should be writeable
        return y

    def copy_from(self, y):
        """Set this state to be a copy of the state `y`"""
        assert self.writeable
        for key in self:
            self[key][...] = y[key]
        self.__dict__.update(y.__dict__, data=self.data)

    def empty(self):
        """Return an uninitialized copy of the state."""
        y = copy.copy(self)
        y.data = copy.copy(self.data)
        for key in self:
            y.data[key] = self[key].empty()
        return y

    @property
    def flat(self):
        """Iterate over the data as if it were a 1D array."""
        return itertools.chain(*(self[key].flat for key in self))

    ######################################################################
    # Default methods using only the __iter__() and __getitem__().  These do
    # not generally need to be overloaded unless customized behavior or
    # performance is required.
    def __len__(self):
        # Use a comprehension here because calling list(self) will call
        # __len__() leading to an infinite loop.  Issue #13.
        return len([_k for _k in self])


class StatesMixin(DataHelperMixin, StateMixin):
    """Mixin for states with a collection (Sequence or Mapping) of data.

    All the user needs to provide is an initialized ``self.data`` which is a
    list or dict of arrays, and then the methods for the required
    `IStateFor...Evolvers`.

    The general interface is provided through the ``__iter__()`` and
    ``__getitem__()`` methods which are assumed to give complete access to the
    data through objects with behave like arrays (i.e. support arithmetic,
    assignment with ``x[...] = y``, and a ``flags.writeable`` attribute.)

    The default implementation is provided by DataHelperMixin which implements
    the required methods in terms of a list or dictionary of states called
    ``self.data``.  These must be overloaded if the states are stored in another way.
    """

    linear = False  # By default assume problems are nonlinear

    def apply(self, expr, **kwargs):
        for key in self:
            kw = {}
            for _k in kwargs:
                kw[_k] = kwargs[_k]
                if isinstance(kw[_k], self.__class__):
                    kw[_k] = kw[_k][key]

            self[key].apply(expr, **kw)

    # The methods implemented here generally assume that the elements
    # state[key] are valid IState objects.
    @property
    def t(self):
        """Time at which state is valid.  This is the time at which potentials
        should be evaluated etc.  (It will be set by the evolvers before
        calling the various functions like compute_dy_dt().)
        """
        # This implementation uses the time of the first element as
        # representative but sets the rest in case they need to refer to the
        # time internally
        key = next(self.__iter__())
        return self[key].t

    @t.setter
    def t(self, t):
        # Ensure that all state t's are set.
        for key in self:
            self[key].t = t

    @property
    def dtype(self):
        """The dtype attribute of States objects is not very well defined since
        each element may in principle have a different type.  For now we assume
        that the first array specifies the dtype of the entire state.
        """
        if "dtype" in self.__dict__:
            dtype = self.__dict__["dtype"]
        else:
            dtype = self[next(self.__iter__())].dtype
        assert any(dtype == self[_k].dtype for _k in self)
        return dtype

    @property
    def writeable(self):
        """Set to `True` if the state is writeable, or `False` if the state
        should only be read.
        """
        return all(self[key].writeable for key in self)

    @writeable.setter
    def writeable(self, value):
        for key in self:
            self[key].writeable = value

    def axpy(self, x, a=1):
        """Perform `self += a*x` as efficiently as possible."""
        assert self.writeable
        for key in self:
            # Can't use += here because python translates that to __setitem__
            # which we do not support
            self[key].__iadd__(x[key] * a)

    def scale(self, f):
        """Perform `self *= f` as efficiently as possible."""
        assert self.writeable
        for key in self:
            self[key].__imul__(f)

    # Note: We do not provide a __setitem__ method because the user should
    # not set items - instead they should be mutated since they may represent
    # data on a GPU or elsewhere.  Thus, usage should be `self[key][...] =`
    # rather than `self[key] =`.  To actually manipulate the data such as when
    # copying, access `self.data` directly.
    def __setitem__(self, key, value):
        """Disable direct setting of items - they should only be mutated.

        If you *need* to set an item, manipulate `self.data` or the equivalent
        directly.
        """
        if key in self:
            msg = "Cannot set `state[{}]`. Did you mean `state[{}][...] = `?".format(
                key, key
            )
        else:
            msg = "Cannot create new item `state[{}]`.".format(key)
        raise TypeError(msg)

    @property
    def __array_interface__(self):
        """Allows states to act as arrays with ``np.asarray(state)``."""

        # It is not simply enough to fail here - since we provide __len__()
        # numpy will try to enumerate through the data.  We must be explicit -
        # we want an array with a single element.
        return dict(shape=(1,), typestr="O", version=3)


@implementer(IStateFlat, IStateAsArray, INumexpr)
class ArrayStateMixin(StateMixin):
    """Mixin providing support for states with a single data array.

    Assumes that the data is an array-like object called `self.data`.  This
    provides all the functionality required by IState.  All the user needs to
    provide are the methods for the required `IStateFor...Evolvers`.
    """

    t = 0.0
    data = None

    @property
    def writeable(self):
        """Set to `True` if the state is writeable, or `False` if the state
        should only be read.
        """
        return self.data.flags.writeable

    @writeable.setter
    def writeable(self, value):
        self.data.flags.writeable = value

    @property
    def dtype(self):
        """Return the dtype of the underlying state.  If this is real, then it
        is assumed that the states will always be real and certain
        optimizations may take place.
        """
        if "dtype" in self.__dict__:
            dtype = self.__dict__["dtype"]
        else:
            dtype = self.data.dtype
        return dtype

    def copy(self):
        """Return a copy of the state.

        Uses `copy.copy()` to shallow-copy attributes, and copy.deepcopy()` to
        copy the data.
        """
        y = copy.copy(self)
        y.data = copy.deepcopy(self.data)
        y.writeable = True  # Copies should be writeable
        return y

    def copy_from(self, y):
        """Set this state to be a copy of the state `y`"""
        assert self.writeable
        self[...] = y[...]
        self.__dict__.update(y.__dict__, data=self.data)

    def empty(self):
        """Return an uninitialized copy of the state."""
        y = copy.copy(self)
        y.data = np.empty_like(self.data)
        y.writeable = True  # Copies should be writeable
        return y

    def zeros(self):
        """Return an uninitialized copy of the state."""
        y = copy.copy(self)
        y.data = np.zeros_like(self.data)
        y.writeable = True  # Copies should be writeable
        return y

    def axpy(self, x, a=1):
        """Perform `self += a*x` as efficiently as possible."""
        assert self.writeable
        self.data += a * x.data

    def scale(self, f):
        """Perform `self *= f` as efficiently as possible."""
        assert self.writeable
        self.data *= f

    # Note: we could get away with __imul__ but it requires one return self
    # which can be a little confusing, so we allow the user to simply define
    # `axpy` and `scale` instead.

    def __repr__(self):
        """We can't really do this since we don't know the constructor.  We
        just show the data here."""
        return "{}(t={}, data={})".format(
            self.__class__.__name__,
            np.array2string(np.array([self.t]))[1:-1],  # Use numpy formatting
            repr(self.data),
        )

    @property
    def flat(self):
        """Iterate over the data as if it were a 1D array."""
        return self.data.flat

    @property
    def __array_interface__(self):
        """Allows states to act as arrays with ``np.asarray(state)``."""
        return self.data.__array_interface__

    def apply(self, expr, **kwargs):
        kw = {}
        for _k in kwargs:
            kw[_k] = kwargs[_k]
            if isinstance(kw[_k], self.__class__):
                kw[_k] = kw[_k].data

        expr(out=self.data, **kw)

    ######################################################################
    # Convenience methods
    def __getitem__(self, key):
        """Provides direct access to the array."""
        return self.data[key]

    def __setitem__(self, key, value):
        """Provides direct access to the array."""
        self.data[key] = value


@implementer(IStateFlat, INumexpr)
class ArraysStateMixin(StatesMixin):
    """Mixin providing support for states with a list or dict of arrays.

    All the user needs to provide is an initialized ``self.data`` which is a
    list or dict of arrays, and then the methods for the required
    `IStateFor...Evolvers`.
    """

    # This class behaves similarly to StatesMixin, but arrays are not states,
    # so any methods that use IState features that are not array features need
    # to be reimplemented
    t = 0.0  # Don't use StatesMixin.t

    @property
    def writeable(self):
        """Set to `True` if the state is writeable, or `False` if the state
        should only be read.
        """
        # We carefully use short-circuiting so that
        # self[key].flags.writeable is only evaluated if 'writeable'
        # is not found.
        return all(
            (
                self[key] if hasattr(self[key], "writeable") else self[key].flags
            ).writeable
            for key in self
        )

    @writeable.setter
    def writeable(self, value):
        for key in self:
            data = self[key]
            if hasattr(data, "writeable"):
                data.writeable = value
            else:
                data.flags.writeable = value

    def empty(self):
        """Return an uninitialized copy of the state."""
        y = copy.copy(self)
        y.data = copy.copy(self.data)
        for key in self:
            y.data[key] = np.empty_like(self[key])
        return y

    def zeros(self):
        """Return an uninitialized copy of the state."""
        y = self.empty()
        for key in self:
            y.data[key] = np.zeros_like(self[key])
        return y

    def apply(self, expr, **kwargs):
        for _l in self:
            kw = {}
            for _k in kwargs:
                kw[_k] = kwargs[_k]
                if isinstance(kw[_k], self.__class__):
                    kw[_k] = kw[_k][_l]

            expr(out=self[_l], **kw)

    @property
    def flat(self):
        """Iterate over the data as if it were a 1D array."""
        return itertools.chain(*(self[key].flat for key in self))


######################################################################
# For backwards compatibility, we inject these objects into interfaces.  We
# can't simply import them in interfaces due to circular dependency issues.
# Also, this is a deprecated feature.
class MultiStateMixin(StatesMixin):
    """Deprecated.  Use StatesMixin"""


for _key in ["StateMixin", "ArrayStateMixin", "ArraysStateMixin", "MultiStateMixin"]:
    _value = locals()[_key]
    setattr(interfaces, _key, _value)
    interfaces.__all__.append(_key)
